let str = 'Hello world!'

console.log(str.length)

let anySubStr = str.substring(1)

console.log(anySubStr)

/* поиск самого длинного слова в предложении */

const sentence = 'Я сегодня иду в кино'

const longerWord1 = (str) => {
  let maxValue = ''

  const mas = str.split(' ')

  mas.forEach((element) => {
    if (element.length > maxValue) {
      maxValue = element.length
    }
  })
  return maxValue
}

console.log(longerWord1(sentence))

const longerWord2 = (str) => {
  const mas = str.split(' ')
  const masLength = mas.map((item) => item.length)

  return Math.max(...masLength)
}

console.log(longerWord2(sentence))
