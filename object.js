/* Сравнение объектов */

/* ПРИМИТИВЫ СРАВНИВАЮТСЯ ПО ЗНАЧЕНИЮ, А ОБЪЕКТЫ ПО ССЫЛКЕ */
const a = {
  name: 'Mike',
}

const b = {
  name: 'Mike',
}

const c = a
console.log(a)

c.name = 'Mike1'

console.log(a)

/* Клонирование объекта */

/* Сопсоб 1 */
let user = {
  name: 'Иван',
  age: 30,
}
let clone1 = {} // новый пустой объект

// скопируем все свойства user в него
for (let key in user) {
  clone1[key] = user[key]
}

console.log(clone1)

/* Сопсоб 2 */

let obj = {
  name: 'course1',
  protocol: 'https',
  maxCount: 10,
  isOnline: true,
  students: ['ivan', 'misha', 'kolya'],
  classroom: {
    teachers: {
      name: 'Oleg',
      surname: 'Popov',
      age: 38,
    },
  },
}

let cloneObj = { ...obj }
cloneObj.classroom = { ...obj.classroom }
cloneObj.classroom.teachers = { ...obj.classroom.teachers }
cloneObj.students = [...obj.students]

cloneObj.classroom.teachers.name = 'Dmitry'
console.log('obj', obj)
console.log('cloneObj', cloneObj)

/* Сопсоб 3 */

let clone3 = Object.assign({})
