// примеры на области видимсоти функций
const exam = () => {
  return 999
}

const res = typeof exam
console.log(res) //function

// func будет доступно только внутри переменно temp
const temp = function func() {
  return 999
}

const res1 = typeof func
console.log(res1) //undefined
