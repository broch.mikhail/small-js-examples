/* поиск совпадений в массиве */
things = new Object()

let qwerty = (things.thing = new Array())

things.thing.push({ place: 'here', name: 'stuff' })
things.thing.push({ place: 'there', name: 'morestuff' })
things.thing.push({ place: 'there', name: 'morestuff' })

console.log(things)

var obj = {}

for (var i = 0, len = things.thing.length; i < len; i++)
  obj[things.thing[i]['place']] = things.thing[i]

things.thing = new Array()
for (var key in obj) things.thing.push(obj[key])

console.log(things)

/* метод ruduce() */
/* Метод reduce() применяет функцию reducer к каждому элементу массива (слева-направо), возвращая одно результирующее значение. */
const array1 = [1, 2, 3, 4]

const sum1 = array1.reduce((accumulator, currentValue) => {
  return accumulator + currentValue
})

console.log(sum1)

/* метод map() */
/* Метод map() создаёт новый массив с результатом вызова указанной функции для каждого элемента массива. */
const array2 = [1, 2, 3, 4]

const sum2 = array2.map((i) => {
  return i + i - 1
})

console.log(sum2)

/* Пересечение массивов */

const mas1 = [1, 2, 3]
const mas2 = [2, 3, 4, 5]

const uniqMas = [...new Set([...mas1, ...mas2])]
console.log('uniqMas', uniqMas)

const uniqMas2 = mas1.filter((i) => mas2.includes(i))
console.log('uniqMas2', uniqMas2)
