// https://blog.askmentor.io/recursive-functions-in-js/
const factorial = (n) => {
  return n !== 1 ? n * factorial(n - 1) : 1
}

console.log(factorial(4))

const reverseString = (str) => {
  return str === ''
    ? ''
    : reverseString(str.substring(1)) + str.charAt(0)
}

console.log(reverseString('reverse'))

// Метод №1: Substring
const reverseString1 = (str) => {
  let reversedString = ''

  /* Проходим по каждому символу в аргументе str
Чтобы развернуть строку, мы присваиваем переменной i значение str.length
Добавляем по очереди каждый символ строки str, начиная с конца, в новую строку.
   */
  for (let i = str.length; i > 0; i--) {
    reversedString += str.substring(i, i - 1)
  }
  return reversedString
}

console.log(reverseString1('reverse'))

// Метод №2: CharAt
const reverseString2 = (str) => {
  let reversedString = ''

  /* с помощью цикла проходим по каждому элементу str
    Чтобы развернуть строку мы присваиваем переменной i значение str.length-1 в то время, как i больше или равно 0.
Добавляем каждый символ, начиная с конца, в новую строку
 */

  for (let i = str.length - 1; i >= 0; i--) {
    reversedString += str.charAt(i)
  }
  return reversedString
}

console.log(reverseString2('reverse'))

const reverseString3 = (str) => {
  /*  string.split() — разбивает каждый символ на индекс массива.
      string.reverse() — разворачивает элементы массива в обратном порядке.
      string.join() — объединяет все элементы массива в строку. */

  return str.split('').reverse().join('')
}

console.log(reverseString3('reverse'))
